

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LifeHealth {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver",
				"C:/Users/User/Desktop/selenium/cucmbertasks/healthwebsite/driver/chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.get("https://www.lifetime.life/");

		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

		WebElement selectClasses = driver.findElement(By.xpath("(//*[contains(text(),'Classes')])[1]"));

		selectClasses.click();

		// WebDriverWait explicitait = new WebDriverWait(driver, 60);

		WebElement selectYoga = driver
				.findElement(By.xpath("(//*[contains(@href,'/fitness-classes/yoga-classes.html')])[1]"));

		selectYoga.click();

		// explicitait.until(ExpectedConditions.elementToBeClickable(selectYoga));

		String yogatitle = driver.getTitle();

		if (yogatitle.equals(driver.getTitle())) {

			System.out.println("you are in " + yogatitle);

		}

		else {
			System.out.println("you are in wrong page ");

		}

		WebElement selectKids = driver.findElement(By.xpath("(//*[@class='ltnav-subitem   has-children'])[4]"));

		selectKids.click();

		String kidstext = selectKids.getText();

		System.out.println(kidstext);

	}

}
